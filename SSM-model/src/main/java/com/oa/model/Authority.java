package com.oa.model;

public class Authority {

    private Integer id;
    private String  aurl;
    private Integer parentId;
    private String title;
    private Integer type;

    //角色表
    private Role roles;

    @Override
    public String toString() {
        return "Authority{" +
                "id=" + id +
                ", aurl='" + aurl + '\'' +
                ", parentId=" + parentId +
                ", title='" + title + '\'' +
                ", type=" + type +
                ", roles=" + roles +
                '}';
    }

    public Role getRoles() {
        return roles;
    }

    public void setRoles(Role roles) {
        this.roles = roles;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAurl() {
        return aurl;
    }

    public void setAurl(String aurl) {
        this.aurl = aurl;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Authority() {

    }
}
