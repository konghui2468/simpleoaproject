package com.oa.model;

import java.util.ArrayList;
import java.util.List;

public class Role {

    private Integer id;
    private String Info;
    private String name;
    private Integer parentId;


    /**
     * 用户权限表
     * @return
     */
    private List<Authority> authority=new ArrayList<>();

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", Info='" + Info + '\'' +
                ", name='" + name + '\'' +
                ", parentId=" + parentId +
                ", authority=" + authority +
                '}';
    }

    public List<Authority> getAuthority() {
        return authority;
    }

    public void setAuthority(List<Authority> authority) {
        this.authority = authority;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInfo() {
        return Info;
    }

    public void setInfo(String info) {
        Info = info;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Role() {

    }
}
