package com.oa.service.Impl;

import com.oa.mapper.AuthorityMapper;
import com.oa.model.Authority;
import com.oa.model.TauthorityVo;
import com.oa.model.ZtreeVo;
import com.oa.service.AuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorityServiceImpl implements AuthorityService {

    @Autowired
    private AuthorityMapper authorityMapper;


    @Override
    public Integer deleteFromRole(Integer rid) {
        return authorityMapper.deleteFromRole(rid);
    }

    @Override
    public Integer InsertAuthority(Authority authority) {
        return authorityMapper.InsertAuthority(authority);
    }

    @Override
    public Integer insertRoleAndAuthorityByAndUidRids(Integer rid, Integer[] aids) {
        return authorityMapper.insertRoleAndAuthorityByAndUidRids(rid,aids);
    }

    @Override
    public List<ZtreeVo> queryRoleZtree(Integer rid) {
        return authorityMapper.queryRoleZtree(rid);
    }

    @Override
    public List<TauthorityVo> QueryAuthorityBytypeAndID() {
        return authorityMapper.QueryAuthorityBytypeAndID();
    }

    @Override
    public Integer DeleteAuthorityById(Integer id) {
        return authorityMapper.DeleteAuthorityById(id);
    }

    @Override
    public TauthorityVo QueryAuthorityById(Integer id) {
        return authorityMapper.QueryAuthorityById(id);
    }

    @Override
    public Integer Count() {
        return authorityMapper.Count();
    }

    @Override
    public List<TauthorityVo> QueryAllAuthority() {
        return authorityMapper.QueryAllAuthority();
    }
}
