package com.oa.service.Impl;

import com.oa.mapper.UserMapper;
import com.oa.model.*;
import com.oa.service.UserService;
import com.oa.utils.EncrypUtil2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;


    @Override
    public Integer updatePhoto(String photo, Integer id) {

        return userMapper.updatePhoto(photo,id);
    }

    @Override
    public Integer deleteFromUser(Integer id) {
        return userMapper.deleteFromUser(id);
    }

    /**
     *添加用户
     */
    @Override
    public Integer AddUser(User user) {
        return userMapper.AddUser(user);
    }

    /**
     * 批量增加
     */
    @Override
    public Integer add(Integer uids, Integer[] rids) {
        return userMapper.insertBatchByAndUidRids(uids,rids);
    }

    /**
     *查询所有用户所有角色
     */
    @Override
    public List<Role> SelectAllRole() {
        return userMapper.SelectAllRole();
    }

    /**
     *删除用户
     */
    @Override
    public Integer deleteUser(Integer id) {
        return userMapper.deleteUser(id);
    }

    /**
     *修改状态/激活用户
     */
    @Override
    public Integer updateFlag(Integer flag, Integer id) {
        return userMapper.updateFlag(flag, id);
    }

    /**
     * 分页模糊查询
     */

    @Override
    public List<TuserRoleVo> queryUserWithRolePagesLike(Integer flag, String nameLike, Integer begin, Integer limit) {
        List<TuserRoleVo> tuserRoleVos = userMapper.queryUserWithRolePagesLike(flag, nameLike, begin, limit);
        for(TuserRoleVo t:tuserRoleVos){
            List<Role> list=t.getRoleList();
            for (Role r:list){
                t.getRids().add(r.getId());
                t.getRnames().add(r.getName());
                t.getRole().add(r.getInfo());
            }
        }

        return tuserRoleVos;
    }


    /**
     *分页查询
     */
    @Override
    public List<TuserRoleVo> queryUserWithRolePages(Integer begin, Integer limit) {
        List<TuserRoleVo> tuserRoleVos = userMapper.queryUserWithRolePages(begin, limit);
        for(TuserRoleVo t:tuserRoleVos){
            List<Role> list=t.getRoleList();
            for (Role r:list){
                t.getRids().add(r.getId());
                t.getRnames().add(r.getName());
                t.getRole().add(r.getInfo());
            }
        }

        return tuserRoleVos;
    }

    @Override
    public List<TauthorityVo> queryMenu(Integer id) {
        /**
         * 查询菜单
          */
        List<TauthorityVo> list = userMapper.queryMenu(id);
        for(TauthorityVo t:list){
            /**
             * 查询子菜单
             */
            t.setChilds(userMapper.queryChildMenu(t.getId()));
        }
        return list;
    }

    @Override
    public Integer updatePassword(String pwd, Integer id) {
         pwd = EncrypUtil2.md5Pass(pwd);
        return userMapper.updatePassword(pwd,id);
    }

    @Override
    public User Login(String name, String pwd) {
         pwd = EncrypUtil2.md5Pass(pwd);
        User user = userMapper.SelectNameAndPwd(name, pwd);
        return user;
    }

    @Override
    public User selectPassword(Integer id) {
        return userMapper.selectPassword(id);
    }
}
