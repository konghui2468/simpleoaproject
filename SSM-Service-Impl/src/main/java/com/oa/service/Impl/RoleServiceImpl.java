package com.oa.service.Impl;

import com.oa.mapper.RoleMapper;
import com.oa.model.Role;
import com.oa.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public Integer AddRole(Role role) {
        return roleMapper.AddRole(role);
    }

    @Override
    public Integer DeleteById(Integer id) {
        return roleMapper.DeleteById(id);
    }

    @Override
    public int UpdateRoleById(Role role) {
        return roleMapper.UpdateRoleById(role);
    }

    @Override
    public Role QueryRoleByID(Integer id) {
        return roleMapper.QueryRoleByID(id);
    }

    @Override
    public List<Role> QueryAllRole() {
        return roleMapper.QueryAllRole();
    }

    @Override
    public Integer Count() {
        return roleMapper.Count();
    }
}
