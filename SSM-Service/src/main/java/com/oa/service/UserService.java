package com.oa.service;

import com.oa.model.Role;
import com.oa.model.TauthorityVo;
import com.oa.model.TuserRoleVo;
import com.oa.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserService {
    User Login(String name,String pwd);

    List<TauthorityVo> queryMenu(Integer id);
    /**
     * 修改图片
     */
    Integer updatePhoto(String photo,Integer id);

    Integer updatePassword(String pwd,Integer id);

    User selectPassword( Integer id);

    /**
     * 分页显示所用户
     */
    List<TuserRoleVo> queryUserWithRolePages(Integer begin,Integer limit);
    /**
     * 分页模糊查询
     */
    List<TuserRoleVo> queryUserWithRolePagesLike(Integer flag,String nameLike,Integer begin,Integer limit);

    /**
     * 修改状态
     */
    Integer updateFlag(Integer flag,Integer id);
    /**
     * 删除用户
     */
    Integer deleteUser(Integer id);
    /**
     * 查询用户所有角色
     */
    List<Role> SelectAllRole();

    /**
     * 批量增加
     */
    Integer add(Integer uids,Integer[] rids);

    Integer deleteFromUser( Integer id);

    /**
     * 添加用户
     */
    Integer AddUser(User user);


}
