package com.oa.service;

import com.oa.model.Role;

import java.util.List;

public interface RoleService {
    /*分页显示所有角色*/
    List<Role> QueryAllRole();


    /*查询总数*/
    Integer Count();

    /*根据角色ID查询角色*/
    Role QueryRoleByID(Integer id);

    /*根据角色ID去修改角色*/
    int UpdateRoleById(Role role);

    /**
     * 删除角色信息
     */
    Integer DeleteById(Integer id);

    /**
     * 添加角色
     */
    Integer AddRole(Role role);
}
