package com.oa.mapper;

import com.oa.model.Role;
import com.oa.model.TauthorityVo;
import com.oa.model.TuserRoleVo;
import com.oa.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
   User SelectNameAndPwd(@Param("name") String name, @Param("password") String pwd);

   /**
    * 查询该用户的一级菜单
    */
   List<TauthorityVo> queryMenu(@Param("uid") Integer id);
   /**
    * 查询该用户的子菜单
    */
   List<TauthorityVo> queryChildMenu(@Param("pid") Integer id);

   /**
    * 修改图片
    */
   Integer updatePhoto(@Param("photo") String photo,@Param("id") Integer id);

   /**
    *修改密码
    */
   Integer updatePassword(@Param("pwd") String pwd,@Param("id") Integer id);

   /**
    * 查询原始密码
    */
   User selectPassword(@Param("id") Integer id);

   /**
    * 分页显示所有用户
    */
   List<TuserRoleVo> queryUserWithRolePages(@Param("begin") Integer begin,@Param("limit") Integer limit);

   /**
    * 分页查询
    */
   List<TuserRoleVo> queryUserWithRolePagesLike(@Param("flag") Integer flag,@Param("no") String no,@Param("begin") Integer begin,@Param("limit") Integer limit);

   /**
    * 修改状态
    */
   Integer updateFlag(@Param("flag") Integer flag,@Param("id") Integer id);

   /**
    * 删除用户
    */
   Integer deleteUser(@Param("id") Integer id);
   /**
    * 查询用户的所有角色
    */
   List<Role> SelectAllRole();
   /**
    * 批量增加
    */
    Integer insertBatchByAndUidRids(@Param("uid") Integer uids,@Param("rids")Integer[] rids);

   Integer deleteFromUser(@Param("id") Integer id);

   /**
    * 添加用户
    */
   Integer AddUser(User user);
}
