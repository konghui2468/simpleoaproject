package com.oa.mapper;

import com.oa.model.Authority;
import com.oa.model.TauthorityVo;
import com.oa.model.ZtreeVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AuthorityMapper {
    /**
     * 查看所有权限
     */
    List<ZtreeVo> queryRoleZtree(Integer rid);
    /**
     *编辑权限
     */
    Integer insertRoleAndAuthorityByAndUidRids(@Param("rid") Integer rid,@Param("aids") Integer[] aids);

    /**
     *批量增加前先删除该角色的所有权限-->
     */
    Integer deleteFromRole(Integer rid);

    /**
     * 分页查询所有权限
     */
    List<TauthorityVo> QueryAllAuthority();

    /*查询总数*/
    Integer Count();
    /**
     * 根据id去查询权限
     */
    TauthorityVo QueryAuthorityById(Integer id);

    /**
     * 删除权限
     */
    Integer DeleteAuthorityById(Integer id);
    /**
     * 将type=1和ParentId=0的权限查询出来-->
     */
    List<TauthorityVo> QueryAuthorityBytypeAndID();
    /**
     * 添加权限
     */
    Integer InsertAuthority(Authority authority);
}
