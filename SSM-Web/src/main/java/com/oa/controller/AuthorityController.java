package com.oa.controller;

import com.github.pagehelper.PageHelper;
import com.oa.model.Authority;
import com.oa.model.ResultVo;
import com.oa.model.TauthorityVo;
import com.oa.model.ZtreeVo;
import com.oa.service.AuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class AuthorityController {

    @Autowired
    private AuthorityService authorityService;

    /**
     * 查看所有权限
     */
    @RequestMapping("/queryroleauthority.do")
    @ResponseBody
    public Object QueryAllAuthority(Integer rid){
        Map<String,Object> map=new HashMap<>();
        List<ZtreeVo> list = authorityService.queryRoleZtree(rid);
        map.put("data",list);
        return map;
    }
    /**
     * 编辑角色权限
     */
    @RequestMapping("/editroleauthority.do")
    @ResponseBody
    public Object UpdateRoleAuthority(Integer rid,Integer[] aids){
        Map<String,Object> map=new HashMap<>();
        Integer delete = authorityService.deleteFromRole(rid);
        if(delete>=0){
            Integer row = authorityService.insertRoleAndAuthorityByAndUidRids(rid, aids);
            if(row>=0){
                map.put("code",0);
                return map;
            }
            else {
                map.put("code",1);
                return map;
            }
        }
        return map;

    }
    /**
     * 查询所有权限
     */
    @RequestMapping("/Authorityall.do")
    @ResponseBody
    public Object QueryAllAuthority(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit){
        ResultVo resultVo=new ResultVo();
        PageHelper.startPage(page,limit);
        List<TauthorityVo> list = authorityService.QueryAllAuthority();
        Integer count = authorityService.Count();
        resultVo.setMsg("");
        resultVo.setData(list);
        resultVo.setCount(count);
        resultVo.setCode(0);
        return  resultVo;
    }
    /**
     * 根据id查询权限
     */
    @RequestMapping("/queryrauthorityByid.do")
    @ResponseBody
    public Object QueryByid(Integer aid){
        TauthorityVo tauthorityVo = authorityService.QueryAuthorityById(aid);
        return tauthorityVo;
    }
     /**
     * 根据删除权限
     */
     @RequestMapping("/Authorityrdel.do")
     @ResponseBody
     public Object DeleteByid(Integer id){
         Map<String,Object> map=new HashMap<>();
         Integer row = authorityService.DeleteAuthorityById(id);
         if(row==1){
             map.put("code",0);
             return map;
         }
         else {
             map.put("code",1);
             return map;
         }
     }
    /**
     * 将type=1和ParentId=0的数回显
     */
    @RequestMapping("/QueryBytypeAndID.do")
    @ResponseBody
    public Object QueryBytypeAndID(){
        List<TauthorityVo> list = authorityService.QueryAuthorityBytypeAndID();
        return list;
    }
    /**
     * 添加权限
     */
    @RequestMapping("/InsertAuthority.do")
    @ResponseBody
    public Object InsertAuthority(Authority authority,Integer sname){
        Map<String,Object> map=new HashMap<>();
        if(sname==null){
            authority.setParentId(0);
            authorityService.InsertAuthority(authority);
            map.put("code",0);
            return map;
        }
        else if(sname!=null) {
            authority.setParentId(sname);
            authorityService.InsertAuthority(authority);
            map.put("code",0);
            return map;

        }
        return map;


    }


}
