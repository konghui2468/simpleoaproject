package com.oa.controller;


import com.oa.model.*;
import com.oa.service.UserService;
import com.oa.utils.EncrypUtil2;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class UserController {

    @Autowired
    private UserService userService;


    @RequestMapping("login.do")
    public String Login(String no,String pass,HttpSession session){
        User user = userService.Login(no, pass);
        if(user==null){
            return "login";
        }
        else {
            session.setAttribute("user",user);
            return "index";
        }
    }
    @RequestMapping("/querySessionUser.do")
    @ResponseBody
    public User Ajax(HttpSession session){
        User user = (User)session.getAttribute("user");
        return  user;
    }
    /**
     * 注销
     */
    @RequestMapping("/userloginout.do")
    public String Loginout(HttpSession session){
      session.invalidate();
      return "login";
    }
    /**
     * 导航栏左边返回Ajax字符串
     */
    @RequestMapping("/usermenu.do")
    @ResponseBody
    public List<TauthorityVo> AjaxMenu(HttpSession session){
        User user = (User)session.getAttribute("user");
        List<TauthorityVo> list = userService.queryMenu(user.getId());
        return list;
    }


    /**
     * 上传图片
     */
    @RequestMapping("/uploadphoto.do")
    @ResponseBody
    public Map<String,Object> updatePhoto( @RequestParam("file") MultipartFile file, HttpServletRequest request) throws IOException {
        //获得登陆的用户信息
        User user = (User) request.getSession().getAttribute("user");

        Map<String, Object> map = new HashMap<>();
        System.out.println(user);
        //上传的目录
        if (!file.isEmpty()) {
        String contextPath = request.getServletContext().getRealPath("/media/headphoto");
        System.out.println(contextPath);

        //上传的文件名
        String filename = file.getOriginalFilename();



        //将图片上传到目录中
        file.transferTo(new File(contextPath + File.separator + filename));

        //更新头像的路径
        String HeadPhoto="media/headphoto/"+"/"+filename;

        userService.updatePhoto(HeadPhoto,user.getId());

        //更新session的头像路径
        user.setHeadphoto(HeadPhoto);
        //判断图片是否存在

            //更改数据库中的图片路径
            map.put("code", 0);
            return map;

        } else {
            map.put("code", 1);
            return map;
        }

    }
    /**
     * 修改密码
     */
    @RequestMapping("/updatepassword.do")
    @ResponseBody
    public Map<String,Object> updatePwd(HttpSession session,String oldpassword,String password){
        Map<String,Object> map=new HashMap<>();
        //查询原始密码
        User user = (User)session.getAttribute("user");
        User user1 = userService.selectPassword(user.getId());
        oldpassword= EncrypUtil2.md5Pass(oldpassword);
        if(!user1.getPassword().equals(oldpassword)){
            map.put("code",1);
             return map;
        }
        else {
            userService.updatePassword(password, user.getId());
            map.put("code",0);
            return map;
        }
    }
    /**
     * 分页查询
     */
    @RequestMapping("/userall.do")
    @ResponseBody
    public Object QueryByPage(@RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue ="10" ) Integer limit
    ,Integer flag,String no){
        Map<String,Object> map=new HashMap<>();
        map.put("code",0);
        map.put("msg","");
        map.put("count",11);
        List<TuserRoleVo> list = userService.queryUserWithRolePages((page-1)*limit, limit);
        map.put("data",list);
        List<TuserRoleVo> list1 = userService.queryUserWithRolePagesLike(flag, no, (page-1)*limit, limit);
        map.put("data",list1);
        return map;
    }

     /**
     * 激活用户
     */
     @RequestMapping("/activeuser.do")
     @ResponseBody
     public ResultVo updateFlag(Integer id,Integer flag) {

         ResultVo resultVo=new ResultVo();

         Integer row = userService.updateFlag(flag, id);

         if(row==0){
             resultVo.setCode(-1);
         }

         return  resultVo;
     }
    /**
     * 删除用户
     */
    @RequestMapping("/userdel.do")
    @ResponseBody
    public Object deleteUser(Integer id){
        Map<String,Object> map=new HashMap<>();
        Integer row = userService.deleteUser(id);
        if(row==1){
            map.put("code",0);
            return map;
        }
        else {
            map.put("code",1);
            return  map;
        }
    }
    /**
     * 查询用户的所有角色
     */
    @RequestMapping("/allroles.do")
    @ResponseBody
    public Object selectAllRole(){
        List<Role> list = userService.SelectAllRole();
        return list;
    }
    /**
     * 批量增加
     */
    @RequestMapping("/userroleedit.do")
    @ResponseBody
    public Object addRole(Integer id,Integer[] rids){
        Map<String,Object> map=new HashMap<>();
        Integer row = userService.deleteFromUser(id);
        if(row>=0){
            Integer row1 = userService.add(id, rids);
            if(row1>=0){
                map.put("code",0);
                return  map;
            }
        }

        else {
            map.put("code",1);
            return map;
        }
        return map;


    }
    /**
     * 添加用户
     */
    @RequestMapping("/AddUser.do")
    @ResponseBody
    public Object adUser(User user,Integer[] rids){
        Map<String,Object> map=new HashMap<>();
        user.setFlag(1);
        System.out.println(rids);
        Integer row = userService.AddUser(user);
        if(row==1){
            Integer add = userService.add(user.getId(), rids);
            if(add>=0){
                map.put("code",0);
                return map;
            }
            else {
                map.put("code",1);
            }

        }
        map.put("code",1);
        return map;

    }

}
