package com.oa.controller;

import com.github.pagehelper.PageHelper;
import com.oa.model.ResultVo;
import com.oa.model.Role;
import com.oa.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class RoleController {

    @Autowired
    private RoleService roleService;


    @RequestMapping("/rolepage.do")
    @ResponseBody
    public Object QueryAllRole(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit){
        //创建一个PageHelper
        PageHelper.startPage(page,limit);
        //创建一个ResultVo对象
        ResultVo resultVo=new ResultVo();
        //查询所有角色
        List<Role> list = roleService.QueryAllRole();
        //查询所有总数
        Integer count = roleService.Count();
        resultVo.setCode(0);
        resultVo.setCount(count);
        resultVo.setData(list);
        resultVo.setMsg("");
        return resultVo;

    }

    /*根据角色ID去查询角色*/
    @RequestMapping("/queryroleByid.do")
    @ResponseBody
    public  Object QueryRoleByID(Integer rid){
        Role role = roleService.QueryRoleByID(rid);
        return role;
    }
    /**
     * 修改角色
     */
    @RequestMapping("/editrole.do")
    @ResponseBody
    public Object UpdateByRid(Role role){
        Map<String,Object> map=new HashMap<>();
        int row = roleService.UpdateRoleById(role);
        if(row==1){
            map.put("code",0);
            return map;
        }
        else {
            map.put("code",1);
            return map;
        }
    }

    /**
     *删除角色
     */
    @RequestMapping("/RoleDel.do")
    @ResponseBody
    public Object DeletById(Integer id){
        Map<String,Object> map=new HashMap<>();
        int row = roleService.DeleteById(id);
        if(row==1){
            map.put("code",0);
            return map;
        }
        else {
            map.put("code",1);
            return map;
        }
    }
    /**
     * 添加角色
     */
    @RequestMapping("/AddRole.do")
    @ResponseBody
    public Object AddRole(Role role){
        Map<String,Object> map=new HashMap<>();
        role.setParentId(0);
        int row = roleService.AddRole(role);
        if(row==1){
            map.put("code",0);
            return map;
        }
        else {
            map.put("code",1);
            return map;
        }
    }

}
